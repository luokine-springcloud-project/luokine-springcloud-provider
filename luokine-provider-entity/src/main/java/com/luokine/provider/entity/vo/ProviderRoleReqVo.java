package com.luokine.provider.entity.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luokine.basic.entity.bean.UserRole;

/**
 * @author: tiantziquan
 * @create: 2019-10-28 16:49
 */
public class ProviderRoleReqVo extends Page<UserRole> {
}
