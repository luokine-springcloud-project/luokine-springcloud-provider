package com.luokine.provider.service.web;

import com.luokine.common.model.Vo.Resp;
import com.luokine.common.model.Vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: tianziquan
 * @create: 2019-12-24 16:22
 */
@RestController
@RequestMapping("/provider")
@Api(tags = "提供者测试接口")
public class ProviderController {

    @GetMapping("/test/gateWay")
    @ApiOperation("测试网关")
    public Resp<String> testGateWay(@RequestParam Integer i){
        return Resp.ok("success:"+i);
    }
}
